#include<stdio.h>

#define PerformanceCost 500
#define PerAttendee 3

int Income(int a);
int Expenditure(int a); 
int profit(int a);
int NoOfAttendees(int a);

//a=Ticket Price  

int	NoOfAttendees(int a){
	return 120-(a-15)/5*20;	
}

int Income(int a){
	return   NoOfAttendees(a)* a;
}

int Expenditure(int a){
	return NoOfAttendees(a)*PerAttendee+PerformanceCost;	
}

int Profit(int a){
	return Income(a)- Expenditure(a);
}

int main(){
	int a;
	printf("Profit for Ticket Prices from Rs.5 to Rs.45: \n\n");
	for(a=5;a<50;a+=5)
	{
		printf("Ticket Price = Rs.%d \t\t Profit = Rs.%d\n",a,Profit(a));
		printf("-----------------------------------------------------\n");
    } 
		return 0;	
	}

	
